var feedeoSource = new function() {
  
  this.player = null; // Player DOM element

  // Embeds the player.
  this.createPlayer = function(container) {
    // Create player.
    var params = { allowScriptAccess: 'always', wmode: 'transparent', allowFullScreen: 'false' };
    var atts = { id: 'player' };
    swfobject.embedSWF('http://www.dailymotion.com/swf?chromeless=1&enableApi=1&playerapiid=player',
                       container, '425', '356', '8', null, null, params, atts);
  }

  // Player events.
  this.onPlayerReady = function(playerid) {
    this.player = document.getElementById('player');
    this.player.addEventListener('onStateChange', 'onDailymotionPlayerStateChange');
    this.player.addEventListener('onError', 'onDailymotionPlayerError');
    this.player.addEventListener('onPlaybackQualityChange', 'onDailymotionPlaybackQualityChange');
    /* TODO: Catch these events.
    this.player.addEventListener('onSeek', 'onDailymotionSeek');
    this.player.addEventListener('onVideoMetadata', 'onDailymotionVideoMetadata');
    this.player.addEventListener('onVideoProgress', 'onDailymotionVideoProgress');
    this.player.addEventListener('onVideoBufferFull', 'onDailymotionVideoBufferFull');
    this.player.addEventListener('onLinearAdStart', 'onDailymotionLinearAdStart');
    this.player.addEventListener('onLinearAdComplete', 'onDailymotionLinearAdComplete');
    this.player.addEventListener('onNonlinearAdStart', 'onDailymotionNonlinearAdStart');
    this.player.addEventListener('onNonlinearAdComplete', 'onDailymotionNonlinearAdComplete');
    */
    feedeoController.onPlayerReady();
  }
  this.onPlayerState = function(state) {
    feedeoController.onPlayerState(state);
  }
  this.onPlayerError = function(error) {
    feedeoController.onPlayerError(error);
  }
  this.onPlayerQualityChange = function(quality) {
    feedeoController.onPlayerQualityChange(quality);
  }

  // Set the dimensions of the player.
  this.setDimensions = function(width, height) {
    $(this.player).width(width).height(height);
  }

  // Player API.
  this.mute = function() {
    this.player.mute();
  }
  this.unMute = function() {
    this.player.unMute();
  }
  this.isMuted = function() {
    return this.player.isMuted();
  }
  this.setQuality = function(quality) {
    this.player.setPlaybackQuality(quality);
  }
  this.loadVideo = function(video) {
    this.player.loadVideoById(video);
  }
  this.getPlayerState = function() {
    return this.player.getPlayerState();
  }
  this.playVideo = function() {
    this.player.playVideo();
  }
  this.pauseVideo = function() {
    this.player.pauseVideo();
  }

  // Source feeds.
  this.getVideosSearchFeed = function(query) {
    return 'https://api.dailymotion.com/videos?search=' + encodeURI(query.terms);
  }
  this.getVideosTagsFeed = function(tags) {
    var query = '';
    $.each(tags, function(i, tag) {
      // TODO: Fix encoding for tags with spaces.
      query += (i > 0 ? ',' : '') + encodeURI(tag);
    });
    return 'https://api.dailymotion.com/videos?tags=' + query;
  }
  this.getUserURL = function(user) {
    return 'http://www.dailymotion.com/' + user;
  }
  this.getUserFeed = function(user) {
    return 'https://api.dailymotion.com/user/' + user;
  }
  this.getUserSubscriptionsFeed = function(user) {
    return 'https://api.dailymotion.com/user/' + user + '/following';
  }
  this.getUserVideosFeed = function(user) {
    return 'https://api.dailymotion.com/user/' + user + '/videos';
  }
  this.getVideoURL = function(video) {
    return 'http://www.dailymotion.com/video/' + video;
  }
  this.getVideoRelatedFeed = function(video) {
    return 'https://api.dailymotion.com/video/' + video + '/related';
  }

  // Source data retrieval.
  this.getUser = function(user, callback, fields) {
    $.ajax({
      type: 'GET',
      url: this.getUserFeed(user),
      data: {
        fields: 'id,screenname,avatar_medium_url,url'
      },
      dataType: 'jsonp',
      cache: true,
      success: function(data) {
        if (data.error) {
          feedeoController.onSourceError(error);
          return;
        }
        var entry = feedeoUtils.map(data, fields, {
          'id': 'id',
          'name': 'screenname',
          'thumbnail' : 'avatar_medium_url',
          'url': 'url'
        });
        callback.apply(feedeoController, [entry]);
      },
      error: function(a,b,c) {
        // TODO: Handle errors.
        console.log(a);
        console.log(b);
        console.log(c);
      }
    }); 
  }
  this.getUserSubscriptions = function(user, callback, fields) {
    var self = this;
    this._getCompleteFeed(this.getUserSubscriptionsFeed(user), function(data, params) {
      $.each(data.list, function(i, entry) {
        self.getUser(entry.id, params.callback, params.fields);
      });
    }, { callback: callback, fields: fields });
  }
  this.getVideos = function(feed, callback, fields, params, pager) {
    pager = pager || { page: 0, totalItems: 0, has_more: true };
    if (!pager.has_more) {
      return;
    }
    $.ajax({
      type: 'GET',
      url: feed,
      data: {
        fields: 'id,title,description,url,thumbnail_medium_url,owner,owner_screenname,owner_url,tags',
        page: pager.page + 1,
      },
      dataType: 'jsonp',
      cache: true,
      success: function(data) {
        if (data.error) {
          feedeoController.onSourceError(data.error);
          return;
        }
        var pager = {
          limit: data.limit,
          page: data.page,
          has_more: data.has_more,
          totalItems: 0
        }
        var videos = [];
        if (data.list) $.each(data.list, function(i, entry) {
          videos.push(feedeoUtils.map(entry, fields, {
            'id': 'id',
            'title': 'title',
            'user': 'owner_screenname',
            'thumbnail': 'thumbnail_medium_url',
            'description': 'description',
            'tags': 'tags',
            'url': 'url',
            'user.url': 'owner_url',
            'user.id': 'owner'
          }));
        });
        callback.apply(feedeoController, [videos, pager, params]);
      },
      error: function(a,b,c) {
        // TODO: Handle errors.
        console.log(a);
        console.log(b);
        console.log(c);
      }
    });
  }

  // Iterate through a paged feed and call the callback on each page.
  this._getCompleteFeed = function(url, callback, params, page) {
    page = page || 1;
    var self = this;
    $.ajax({
      type: 'GET',
      url: url,
      data: {
        page: page
      },
      dataType: 'jsonp',
      cache: true,
      success: function(data) {
        if (data.error) {
          feedeoController.onSourceError(error);
          return;
        }
        callback.apply(self, [data, params]);
        if (data.has_more) {
          self._getCompleteFeed(url, callback, params, page + 1);
        }
      },
      error: function(a,b,c) {
        // TODO: Handle errors.
        console.log(a);
        console.log(b);
        console.log(c);
      }
    });
  }

} // feedeoSource

function onDailymotionPlayerReady(playerid) {
  feedeoSource.onPlayerReady(playerid);
}

function onDailymotionPlayerStateChange(state) {
  feedeoSource.onPlayerState(state);
}

function onDailymotionPlayerError(error) {
  feedeoSource.onPlayerError(error);
}

function onDailymotionPlaybackQualityChange(quality) {
  feedeoSource.onPlayerQualityChange(quality);
}

