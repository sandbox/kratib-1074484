var feedeoController = new function() {

  this.context = null; // DOM element acting as host to the controller
  this.settings = null; // Feedeo options
  this.stack = []; // Stack of watched channels
  this.histoire = []; // History of watched videos
  this.channels = []; // List of source channels

  // Initialize Feedeo. Called when context (host) is ready.
  this.init = function(context, settings) {
    this.context = context;
    this.settings = settings;

    // Create player.
    feedeoSource.createPlayer('player');

    // Load user subscriptions.
    feedeoSource.getUserSubscriptions(this.settings.user, this.loadUserSubscription, ['name', 'thumbnail']);

    // Load saved channels.
    this.loadSavedChannels();

    // Respond to UI events.
    var self = this;
    $('#controls #previous', this.context).click(function() {
      self.previousVideo();
    });
    $('#controls #next', this.context).click(function() {
      self.nextVideo();
    });
    $('#controls #pause', this.context).click(function() {
      self.pauseVideo();
    });
    $('#controls #mute', this.context).click(function() {
      self.muteVideo();
    });
    $('#controls #pop', this.context).click(function() {
      self.previousFeed();
    });
    $('#controls #query', this.context).keyup(function() {
      self.onQueryChange();
    });
    $('#controls #search', this.context).click(function() {
      self.watchSearch();
    });
    $('#controls #bookmark', this.context).click(function() {
      self.enableBookmark();
    });
    $('#controls #bookmark-save', this.context).click(function() {
      self.saveBookmark();
    });
    $('#controls #bookmarks', this.context).change(function() {
      self.gotoBookmark();
    });
    $('#status #feed-button', this.context).click(function() {
      self.watchFeed();
    });
    $('#status #save', this.context).keyup(function() {
      self.onSaveChange();
    });
    $('#status #save-button', this.context).click(function() {
      self.saveFeed();
    });
    $('#status #related-button', this.context).click(function() {
      self.watchRelated();
    });
    $('#status #user-button', this.context).click(function() {
      self.watchUser();
    });
    $('#status #tags-button', this.context).click(function() {
      self.watchTags();
    });
    $('#status #saved', this.context).change(function() {
      self.watchSaved();
    });
    $('#playlist #more-button', this.context).click(function() {
      self.nextPage(false);
    });
  }

  // Start watching. Called when player is ready.
  this.onPlayerReady = function() {
    var width = $('#feedeo', this.context).width();
    var height = width * 9 / 16;
    feedeoSource.setDimensions(width, height);
    feedeoSource.setQuality(this.getQuality(width, height));
    feedeoSource.mute();

    // Start watching Drupal.
    this.watchNew(this.settings.feed || feedeoSource.getVideosSearchFeed({ terms: 'drupal' }));
  }

  this.getQuality = function(width, height) {
    if (height <= 240) {
      quality = 'small';
    }
    else if (height <= 360) {
      quality = 'medium';
    }
    else if (height <= 480) {
      quality = 'large';
    }
    else if (height <= 720) {
      quality = 'hd720';
    }
    else if (height <= 1080) {
      quality = 'hd1080';
    }
    else {
      quality = 'highres';
    }
    return quality;
  }

  // Load a user/channel subscription. Called back from feedeoSource.
  this.loadUserSubscription = function(channel) {
    this.channels.push(channel);
    $('#channels ul', this.context).append('<li><img class="feedeo-channel" title="' + channel.name + '" onclick="feedeoController.watchChannel(' + (this.channels.length-1) + ')" src="' + channel.thumbnail  + '">');
  }

  // Play current video.
  this.playVideo = function() {
    var stack = this.stack[this.stack.length-1];
    var videos = stack.videos;
    var currentIndex = stack.current;
    var url = stack.url;
    var item = videos[currentIndex];

    // Save history.
    if (this.histoire[item.id] === undefined) {
      this.histoire[item.id] = {
        item: item // video item
      }
      $('#history ul', this.context).append('<li><img class="feedeo-video" title="' + item.title + '" src="' + item.thumbnail + '" onclick="feedeoController.playHistory(\'' + item.id + '\')"></li>');
    }

    // Update UI.
    $('#status #index', this.context).html((currentIndex+1) + " of " + stack.pager.totalItems);
    $('#status #title', this.context).html('<a target="_blank" href="'+ feedeoSource.getVideoURL(item.id) + '">' + item.title + '</a>');
    $('#status #description', this.context).html(item.description);
    $('#status #user', this.context).html('<a target="_blank" href="' + feedeoSource.getUserURL(item.user) + '">' + item.user + "</a>");
    $('#status #watch-user', this.context).removeAttr('disabled');
    $('#status #feed', this.context).val(url);
    var tags = '';
    if (item.tags) {
      $.each(item.tags, function(i, tag) {
        tags += '<input type="checkbox" value="' + tag  + '">' + tag + '</input>';
      });
    }
    $('#status #tags', this.context).html(tags);
    $('#status #watch-tags', this.context).removeAttr('disabled');
    if (currentIndex >= stack.pager.totalItems) {
      $('#controls #next', this.context).attr('disabled', 'disabled');
    }
    else {
      $('#controls #next', this.context).removeAttr('disabled');
    }
    if (this.stack.length > 1) {
      $("#controls #pop", this.context).removeAttr('disabled');
    }
    else {
      $("#controls #pop", this.context).attr('disabled', 'disabled');
    }
    if (currentIndex > 0 || this.stack.length > 1) {
      $('#controls #previous', this.context).removeAttr('disabled');
    }
    else {
      $('#controls #previous', this.context).attr('disabled', 'disabled');
    }
    $('#controls #mute', this.context).removeAttr('disabled');
    $('#controls #bookmark', this.context).removeAttr('disabled');
    $('#controls #bookmark-save', this.context).attr('disabled', 'disabled');
    $('#playlist img[id!="item-' + currentIndex + '"]', this.context).css('border', '0');
    $('#playlist #item-' + currentIndex, this.context).css('border', '5px solid');

    // Load bookmarks for this video.
    this.loadBookmarks(item.id);

    // Play the video.
    feedeoSource.loadVideo(item.id);

    // Should we get more videos?
    if (currentIndex >= videos.length - 10) {
      this.nextPage(false);
    }
  }

  // Play previous video.
  this.playHistory = function(id) {
    var item = this.histoire[id].item;
    feedeoSource.loadVideo(item.id);
  }

  // Activate previous channel in stack.
  this.previousFeed = function() {
    this.stack.pop();
    this.refreshPlaylist();
    this.playVideo();
  }

  // Mute video.
  this.muteVideo = function() {
    if (feedeoSource.isMuted()) {
      feedeoSource.unMute();
      $('#controls #mute', this.context).html('Mute');
    }
    else {
      feedeoSource.mute();
      $('#controls #mute', this.context).html('Unmute');
    }
  }

  // Advance to next video.
  this.nextVideo = function() {
    var videos = this.stack[this.stack.length-1].videos;

    this.stack[this.stack.length-1].current++;
    if (this.stack[this.stack.length-1].current < videos.length) {
      this.playVideo();
    }
    else {
      this.nextPage();
    }
  }

  // Return to previous video.
  this.previousVideo = function() {
    var videos = this.stack[this.stack.length-1].videos;
    if (this.stack[this.stack.length-1].current > 0) {
      this.stack[this.stack.length-1].current--;
      this.playVideo();
    }
    else if (this.stack.length > 1) {
      this.previousFeed();
    }
  }

  // Fetch next page of videos.
  this.nextPage = function(doPlay) {
    doPlay = typeof(doPlay) == 'undefined' ? true : doPlay;
    this.getPage(doPlay);
  }

  // Get page of videos.
  this.getPage = function(doPlay) {
    doPlay = typeof(doPlay) == 'undefined' ? true : doPlay;
    feedeoSource.getVideos(this.stack[this.stack.length-1].url, this.loadPage, ['id', 'title', 'user', 'thumbnail', 'description', 'tags'], { doPlay: doPlay }, this.stack[this.stack.length-1].pager);
  }

  // Load page of videos. Called back from feedeoSource.
  this.loadPage = function(videos, pager, params) {
    this.stack[this.stack.length-1].pager = pager;
    if (this.stack[this.stack.length-1].videos.length) {
      var startIndex = this.stack[this.stack.length-1].videos.length;
      this.stack[this.stack.length-1].videos  = this.stack[this.stack.length-1].videos.concat(videos);
      this.refreshPlaylist(startIndex);
      if (params.doPlay) {
        this.playVideo();
      }
    }
    else {
      if (videos.length) {
        this.stack[this.stack.length-1].videos = videos;
        this.stack[this.stack.length-1].current = 0;
        this.refreshPlaylist();
        if (params.doPlay) {
          this.playVideo();
        }
      }
      else {
        this.stack.pop();
      }
    }
  }

  // Refresh video playlist.
  this.refreshPlaylist = function(startIndex) {
    startIndex = startIndex || 0;
    var videos = this.stack[this.stack.length-1].videos;
    if (!startIndex) {
      $("#playlist ul", this.context).html('');
    }
    var self = this;
    $.each(videos, function(i, item) {
      if (i >= startIndex) {
        $("#playlist ul", self.context).append('<li><img id="item-' + i + '" class="feedeo-video" src="' + item.thumbnail + '" onclick="feedeoController.watchVideo(' + i + ');" title="' + item.title + '"></li>');
      }
    });
  }

  // Watch a video.
  this.watchVideo = function(i) {
    this.stack[this.stack.length-1].current = i;
    this.playVideo();
  }

  // Pause/resume current video.
  this.pauseVideo = function() {
    switch (feedeoSource.getPlayerState()) {
      case this.playerStates.playing:
        feedeoSource.pauseVideo();
        break;
      case this.playerStates.paused:
        feedeoSource.playVideo();
        break;
    }
  }

  // Bookmarking.
  this.enableBookmark = function() {
    this.bookmarkTime = feedeoSource.getCurrentTime();
    feedeoSource.pauseVideo();
    $('#controls #bookmark-save', this.context).removeAttr('disabled');
  }
  this.saveBookmark = function() {
    $('#controls #bookmark-save', this.context).attr('disabled', 'disabled');

    var stack = this.stack[this.stack.length-1];
    var videos = stack.videos;
    var currentIndex = stack.current;
    var url = stack.url;
    var item = videos[currentIndex];

    var comment = $('#controls #bookmark-comment', this.context).val();
    var video = item.id;
    var time = this.bookmarkTime;
    var title = time + " - " + comment.trunc(25, true);
    var self = this;

    $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + 'feedeo/js/bookmarks/add',
      data: {
        title: title,
        comment: comment,
        video: video,
        time: time
      },
      datatype: 'json',
      async: false,
      success: function(data) {
        var result = Drupal.parseJson(data);
        $('#controls #bookmarks', self.context)
          .append($("<option></option>")
            .attr("value", result.time)
            .attr("title", result.comment)
            .text(result.title))
          .sortSelect()
          .val(result.time);
      }
    });

    feedeoSource.playVideo();
  }

  // Watch new feed.
  this.watchNew = function(url) {
    this.stack.push({
      url: url,               // feed URL
      videos: [],             // videos gathered so far
      current: 0,             // currently playing video
      pager: null             // pager used by feedeoSource
    });
    this.nextPage();
  }

  // Watch user channel.
  this.watchChannel = function(i) {
    var entry = this.channels[i];
    this.watchNew(feedeoSource.getUserVideosFeed(entry.name));
  }

  // Watch user channel.
  this.watchUser = function() {
    var videos = this.stack[this.stack.length-1].videos;
    var current = this.stack[this.stack.length-1].current;
    this.watchNew(feedeoSource.getUserVideosFeed(videos[current].user));
  }

  // Watch related videos channel.
  this.watchRelated = function() {
    var videos = this.stack[this.stack.length-1].videos;
    var current = this.stack[this.stack.length-1].current;
    this.watchNew(feedeoSource.getVideoRelatedFeed(videos[current].id));
  }

  // Watch tags channel.
  this.watchTags = function() {
    var tags = [];
    $('#status #tags input:checkbox:checked', this.context).each(function() {
      tags.push($(this).val());
    });
    this.watchNew(feedeoSource.getVideosTagsFeed(tags));
  }

  // Watch search channel.
  this.watchSearch = function() {
    this.watchNew(feedeoSource.getVideosSearchFeed({ terms: $('#controls #query', this.context).val() }));
  }

  // Watch feed channel.
  this.watchFeed = function() {
    this.watchNew($('#status #feed', this.context).val());
  }

  // Watch saved channel.
  this.watchSaved = function() {
    var feed = $('#status #saved', this.context).val();
    if (feed) {
      this.watchNew(feed);
    }
  }

  // Save channel.
  this.saveFeed = function() {
    var videos = this.stack[this.stack.length-1].videos;
    var title = $('#status #save', this.context).val();
    var description = '';
    var feed = this.stack[this.stack.length-1].url;
    var source = this.settings.source;
    var self = this;

    $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + 'feedeo/js/channels/add',
      data: {
        title: title,
        description: description,
        feed: feed,
        source: source
      },
      datatype: 'json',
      async: false,
      success: function(data) {
        var result = Drupal.parseJson(data);
        $('#status #saved', self.context).
          append($("<option></option>").
          attr("value", result.nid).
          text(result.title));
        $('#status #saved', self.context).val(result.nid);
      }
    });
  }

  // Load channels.
  this.loadSavedChannels = function() {
    var self = this;
    $.ajax({
      type: 'GET',
      url: Drupal.settings.basePath + 'feedeo/js/channels/get',
      data: {
        source: this.settings.source
      },
      success: function(data) {
        var result = Drupal.parseJson(data);
        $.each(result, function(i, entry) {
          $('#status #saved', self.context).
            append($("<option></option>").
              attr('value', entry.feed).
              text(entry.title)
            );
        });
      }
    });
  }

  // Jump to bookmark.
  this.gotoBookmark = function() {
    var time = 0 + $('#controls #bookmarks', this.context).val();
    if (time > 0) {
      feedeoSource.seekTo(time);
    }
  }

  // Load bookmarks.
  this.loadBookmarks = function(id) {
    var self = this;
    $('#controls #bookmarks')
      .find('option')
      .remove()
      .end()
      .append('<option value="0">- BOOKMARKS -</option>');
    $.ajax({
      type: 'GET',
      url: Drupal.settings.basePath + 'feedeo/js/bookmarks/get',
      data: {
        video: id
      },
      success: function(data) {
        var bookmarks = Drupal.parseJson(data);
        $.each(bookmarks, function(i, bookmark) {
          $('#controls #bookmarks', self.context).
            append($("<option></option>").
              attr('value', bookmark.time).
              attr('title', bookmark.comment).
              text(bookmark.title)
            );
        });
      }
    });
  }

  // Respond to change on channel name input.
  this.onSaveChange = function() {
    var value = $('#status #save', this.context).val();
    if (value.length > 0) {
      $('#status #save-button', this.context).removeAttr('disabled');
    }
    else {
      $('#status #save-button', this.context).attr('disabled', 'disabled');
    }
  }

  // Respond to change on query input.
  this.onQueryChange = function() {
    var value = $("#controls #query", this.context).val();
    if (value.length > 0) {
      $("#controls #search", this.context).removeAttr('disabled');
    }
    else {
      $("#controls #search", this.context).attr('disabled', 'disabled');
    }
  }

  this.playerStates = {
    unstarted: -1,
    ended: 0,
    playing: 1,
    paused: 2,
    buffering: 3,
    cued: 5
  }

  // Respond to player changing state.
  this.onPlayerState = function(state) {
    switch (state) {
    case this.playerStates.ended:
      this.nextVideo();
      break;
    case this.playerStates.playing:
      $("#controls #pause", this.context).removeAttr('disabled').html("Pause");
      break;
    case this.playerStates.paused:
      $("#controls #pause", this.context).removeAttr('disabled').html("Play");
      break;
    default:
      $("#controls #pause", this.context).attr('disabled', 'disabled');
      break;
    }
  }

  // Respond to player throwing error.
  this.onPlayerError = function(error) {
    this.nextVideo();
  }

  // Respond to player changing playback quality.
  this.onPlayerQualityChange = function(quality) {
  }

  // Respond to feed error.
  this.onSourceError = function(error) {
    $("#messages-error ul", this.context).append('<li>Error ' + error.code +  ' (' + error.type + '): ' + error.message + '</li>');
  }

} // feedeoController

var feedeoUtils = {

  map: function(data, fields, map) {
    var entry = {};
    $.each(fields, function(i, field) {
      if (typeof(map[field]) != 'undefined') {
        if (typeof(map[field]) == 'function') {
          entry[field] = map[field](data);
        }
        else {
          try {
            entry[field] = eval('data.' + map[field]);
          }
          catch (err) {
            // TODO: Log missing field in data.
          }
        }
      }
      else {
        // TODO: Log the missing field in map.
      }
    });
    entry._source = data; // Keep original
    return entry;
  }

} // feedeoUtils

Drupal.behaviors.feedeo = function(context) {
  feedeoController.init(context, Drupal.settings.feedeo);
}

// @see http://www.wrichards.com/blog/2009/02/jquery-sorting-elements/
jQuery.fn.sort = function() {
   return this.pushStack( [].sort.apply( this, arguments ), []);
}

// @see http://stackoverflow.com/a/11196096/209184
jQuery.fn.sortSelect = function() {
    var op = this.children("option");
    op.sort(function(a, b) {
      return Number(a.value) > Number(b.value) ? 1 : -1;
    })
    return this.empty().append(op);
}

// @see http://stackoverflow.com/a/1199420/209184
String.prototype.trunc = function(n, useWordBoundary) {
  var tooLong = this.length > n,
      s_ = tooLong ? this.substr(0,n-1) : this;
  s_ = useWordBoundary && tooLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
  return tooLong ? s_ + '...' : s_;
}
