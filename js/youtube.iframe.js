var feedeoSource = new function() {

  this.player = null; // Player DOM element
  this.container = null;

  // Embeds the player.
  this.createPlayer = function(container) {
    this.container = container;
    var tag = document.createElement('script');
    tag.src = "http://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  // Player events.
  this.onPlayerReady1 = function() {
    this.player = new YT.Player(this.container, {
      playerVars: {
        'enablejsapi': 1,
        'autoplay' : 0,
        'fs': 0,
        'controls': 0
      },
      events: {
        'onReady': onYouTubePlayerReady,
        'onStateChange': onYouTubePlayerStateChange,
        'onPlaybackQualityChange': onYouTubePlaybackQualityChange,
        'onError': onYouTubePlayerError
      }
    });
  }
  this.onPlayerReady2 = function(player) {
    feedeoController.onPlayerReady();
  }
  this.onPlayerState = function(state) {
    feedeoController.onPlayerState(state);
  }
  this.onPlayerError = function(error) {
    feedeoController.onPlayerError(error);
  }
  this.onPlayerQualityChange = function(quality) {
    feedeoController.onPlayerQualityChange(quality);
  }

  // Set the dimensions of the player.
  this.setDimensions = function(width, height) {
    $('#'+this.container).width(width).height(height);
  }

  // Player API.
  this.mute = function() {
    this.player.mute();
  }
  this.unMute = function() {
    this.player.unMute();
  }
  this.isMuted = function() {
    return this.player.isMuted();
  }
  this.setQuality = function(quality) {
    this.player.setPlaybackQuality(quality);
  }
  this.loadVideo = function(video) {
    this.player.loadVideoById(video);
  }
  this.getPlayerState = function() {
    return this.player.getPlayerState();
  }
  this.playVideo = function() {
    this.player.playVideo();
  }
  this.pauseVideo = function() {
    this.player.pauseVideo();
  }
  this.getCurrentTime = function() {
    return this.player.getCurrentTime();
  }
  this.seekTo = function(time) {
    this.player.seekTo(time, true);
  }

  // Source feeds.
  this.getVideosSearchFeed = function(query) {
    return 'http://gdata.youtube.com/feeds/api/videos?q=' + encodeURI(query.terms);
  }
  this.getVideosTagsFeed = function(tags) {
    var query = '';
    $.each(tags, function(i, tag) {
      // TODO: Fix encoding for tags with spaces.
      query += '/' + encodeURI(tag);
    });
    return 'http://gdata.youtube.com/feeds/api/videos/-' + query;
  }
  this.getUserURL = function(user) {
    return 'http://youtube.com/user/' + user;
  }
  this.getUserFeed = function(user) {
    return 'http://gdata.youtube.com/feeds/api/users/' + user;
  }
  this.getUserSubscriptionsFeed = function(user) {
    return 'http://gdata.youtube.com/feeds/api/users/' + user + '/subscriptions';
  }
  this.getUserVideosFeed = function(user) {
    return 'http://gdata.youtube.com/feeds/api/users/' + user + '/uploads';
  }
  this.getVideoURL = function(video) {
    return 'http://youtube.com/watch?v=' + video;
  }
  this.getVideoRelatedFeed = function(video) {
    return 'http://gdata.youtube.com/feeds/api/videos/' + video + '/related';
  }

  // Source data retrieval.
  this.getUser = function(user, callback, fields) {
    $.ajax({
      type: 'GET',
      url: this.getUserFeed(user),
      data: {
        v: 2,
        alt: 'json'
      },
      dataType: 'jsonp',
      success: function(data) {
        if (data.error) {
          feedeoController.onSourceError(error);
          return;
        }
        var entry = feedeoUtils.map(data.entry, fields, {
          'name': 'yt$username.$t',
          'thumbnail' : 'media$thumbnail.url'
        });
        callback.apply(feedeoController, [entry]);
      },
      error: function(a,b,c) {
        // TODO: Handle errors.
        console.log(a);
        console.log(b);
        console.log(c);
      }
    });
  }
  this.getUserSubscriptions = function(user, callback, fields) {
    var self = this;
    this._getCompleteFeed(this.getUserSubscriptionsFeed(user), function(data, params) {
      $.each(data.feed.entry, function(i, entry) {
        self.getUser(entry['yt$username']['$t'], params.callback, params.fields);
      });
    }, { callback: callback, fields: fields });
  }
  this.getVideos = function(feed, callback, fields, params, pager) {
    pager = pager || { itemsPerPage: 0, startIndex: 1, totalItems: 0 };
    if (pager.totalItems) {
      if (pager.startIndex + pager.itemsPerPage >= pager.totalItems) {
        return;
      }
      else {
        pager.startIndex += pager.itemsPerPage;
      }
    }
    $.ajax({
      type: 'GET',
      url: feed,
      data: {
        format: 5,
        v: 2,
        alt: 'jsonc',
        'start-index': pager.startIndex
      },
      dataType: 'jsonp',
      success: function(data) {
        if (data.error) {
          feedeoController.onSourceError(error);
          return;
        }
        var pager = {
          itemsPerPage: data.data.itemsPerPage,
          startIndex: data.data.startIndex,
          totalItems: data.data.totalItems
        }
        var videos = [];
        if (data.data.items) $.each(data.data.items, function(i, entry) {
          if (typeof(entry.video) != 'undefined') { // support playlists
            entry = entry.video;
          }
          videos.push(feedeoUtils.map(entry, fields, {
            'id': 'id',
            'title': 'title',
            'user': 'uploader',
            'thumbnail': 'thumbnail.sqDefault',
            'description': 'description',
            'tags': 'tags'
          }));
        });
        callback.apply(feedeoController, [videos, pager, params]);
      },
      error: function(a,b,c) {
        // TODO: Handle errors.
        console.log(a);
        console.log(b);
        console.log(c);
      }
    });
  }

  // Iterate through a paged feed and call the callback on each page.
  this._getCompleteFeed = function(url, callback, params, startIndex) {
    startIndex = startIndex || 1;
    var self = this;
    $.ajax({
      type: 'GET',
      url: url,
      data: {
        v: 2,
        alt: 'json-in-script',
        'start-index': startIndex
      },
      dataType: 'jsonp',
      success: function(data) {
        if (data.error) {
          feedeoController.onSourceError(error);
          return;
        }
        callback.apply(self, [data, params]);
        startIndex += data.feed['openSearch$itemsPerPage']['$t'];
        if (startIndex < data.feed['openSearch$totalResults']['$t']) {
          self._getCompleteFeed(url, callback, params, startIndex);
        }
      },
      error: function(a,b,c) {
        // TODO: Handle errors.
        console.log(a);
        console.log(b);
        console.log(c);
      }
    });
  }

} // feedeoSource

function onYouTubePlayerAPIReady() {
  feedeoSource.onPlayerReady1();
}

function onYouTubePlayerReady(event) {
  feedeoSource.onPlayerReady2(event.target);
}

function onYouTubePlayerStateChange(event) {
  feedeoSource.onPlayerState(event.data);
}

function onYouTubePlayerError(error) {
  feedeoSource.onPlayerError(error);
}

function onYouTubePlaybackQualityChange(quality) {
  feedeoSource.onPlayerQualityChange(quality);
}

