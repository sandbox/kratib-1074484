// $Id$

Drupal.behaviors.feedeo_Vimeo = function(context) {
  // Create player.
  var params = { allowScriptAccess: "always", wmode: "transparent", allowFullScreen: "false" };
  var atts = { id: "myvimeoplayer" };
  var flashVars = {
    clip_id: '1241590',
    server: 'vimeo.com',
    js_api: 1
  }
  swfobject.embedSWF("http://vimeo.com/moogaloop.swf", 
                     "player", "425", "356", "8", null, flashVars, params, atts);

  // Load user data.
  //getCompleteFeed('http://gdata.youtube.com/feeds/api/users/' + Drupal.settings.feedeo.youtube.user + '/subscriptions', 1, loadProfile);

  // Load saved channels.
  $.ajax({
    type: 'GET',
    url: Drupal.settings.basePath + 'feedeo/js/get',
    data: {
      source: 'youtube'
    },
    success: function(data) {
      var result = Drupal.parseJson(data);
      $.each(result, function(i, entry) {
        $('#saved').
          append($("<option></option>").
          attr("value", entry['feed']).
          text(entry['title']));
      });
    }
  });

  // Respond to chosen saved channel.
  $('#saved').change(function() {
    var feed = $(this).val();
    if (feed) {
      watchNew(feed);
    }
  });
}

var stack = new Array();
var histoire = new Array();
var player;
var channels = null;

function getCompleteFeed(url, startIndex, callback) {
  $.ajax({
    type: 'GET',
    url: url,
    data: {
      v: 2,
      alt: 'json-in-script',
      'start-index': startIndex
    },
    dataType: 'jsonp',
    success: function(data) {
      callback(data);
      startIndex += data.feed['openSearch$itemsPerPage']['$t'];
      if (startIndex < data.feed['openSearch$totalResults']['$t']) {
        getCompleteFeed(url, startIndex, callback);
      }
    },
    error: function(a,b,c) {
    }
  });
}

function loadProfile(data) {
  if (channels == null) {
    channels = data;
  }
  else {
    channels.feed.entry = channels.feed.entry.concat(data.feed.entry);
    channels.feed['openSearch$itemsPerPage']['$t'] += data.feed['openSearch$itemsPerPage']['$t'];
  }
  $.each(data.feed.entry, function(i, entry) {
    $.ajax({
      type: 'GET',
      url: 'http://gdata.youtube.com/feeds/api/users/' + entry['yt$username']['$t'],
      data: {
        v: 2,
        alt: 'json-in-script',
      },
      dataType: 'jsonp',
      success: function(profile) {
        var j = data.feed['openSearch$startIndex']['$t'] + i - 1;
        $('#channels ul').append('<li><img title="' + entry.title['$t'] + '" onclick="watchChannel(' + j + ')" src="' + profile.entry['media$thumbnail'].url  + '" />');
      },
      error: function(a,b,c) {
      }
    });
  });
}

function playVideo() {
  var videos = stack[stack.length-1].videos;
  var current = stack[stack.length-1].current;
  var url = stack[stack.length-1].url;
  var currentIndex = ((videos.page - 1) * videos.perpage) + current;
  var item = videos.video[current];

  $.ajax({
    type: 'GET',
    url: 'http://vimeo.com/api/v2/video/' + item.id  + '.json',
    data: {
    },
    success: function(data) {
      console.log(data);
      return;
      // Save history.
      if (histoire[item.id] === undefined) {
        histoire[item.id] = {
          item: item,
          url: url,
          current: currentIndex,
        };
        $('#history ul').append('<li><img id="history-' + item.id + '" title="' + item.title + '" src="' + item.thumbnail.sqDefault + '" onclick="playHistory(\'' + item.id + '\')" /></li>');
      }

      // Update UI.
      $('#status #index').html(currentIndex + " of " + videos.data.totalItems);
      $('#status #title').html('<a target="_blank" href="http://youtube.com/watch?v=' + item.id + '">' + item.title + '</a>');
      $('#status #description').html(item.description);
      $('#status #user').html('<a target="_blank" href="http://youtube.com/user/' + item.uploader + '">' + item.uploader + "</a>");
      $('#status #watch-user').removeAttr('disabled');
      $('#status #feed').val(url);
      var tags = '';
      if (item.tags) {
        $.each(item.tags, function(i, tag) {
          tags += '<input type="checkbox" value="' + tag  + '" onchange="">' + tag + '</input>';
        });
      }
      $('#status #tags').html(tags);
      $('#status #watch-tags').removeAttr('disabled');
      if (currentIndex >= videos.data.totalItems) {
        $('#controls #next').attr('disabled', 'disabled');
      }
      else {
        $('#controls #next').removeAttr('disabled');
      }
      if (stack.length > 1) {
        $("#controls #pop").removeAttr('disabled');
      }
      else {
        $("#controls #pop").attr('disabled', 'disabled');
      }
      if (current > 0 || videos.data.startIndex > 1 || stack.length > 1) {
        $('#controls #previous').removeAttr('disabled');
      }
      else {
        $('#controls #previous').attr('disabled', 'disabled');
      }
      $('#controls #mute').removeAttr('disabled');
      $('#playlist img[id!="item-' + currentIndex + '"]').css('border', '0');
      $('#playlist #item-' + currentIndex).css('border', '5px solid');

      player.loadVideoById(item.id);

      // Should we get more videos?
      if (current >= videos.data.items.length - 10) {
        nextPage(false);
      }
    }
  });
}

function playHistory(id) {
  var item = histoire[id].item;
  player.loadVideoById(item.id);
}

function previousFeed() {
  stack.pop();
  refreshPlaylist();
  playVideo();
}

function muteVideo() {
  if (player.isMuted()) {
    player.unMute();
    $('#controls #mute').html('Mute');
  }
  else {
    player.mute();
    $('#controls #mute').html('Unmute');
  }
}

function nextVideo() {
  var videos = stack[stack.length-1].videos;

  stack[stack.length-1].current++;
  if (stack[stack.length-1].current < videos.data.items.length) {
    playVideo();
  }
  else {
    nextPage();
  }
}

function previousVideo() {
  var videos = stack[stack.length-1].videos;
  if (stack[stack.length-1].current > 0) {
    stack[stack.length-1].current--;
    playVideo();
  }
  else if (videos.data.startIndex > 1) {
    previousPage();
  }
  else if (stack.length > 1) {
    previousFeed();
  }
}

function nextPage(doPlay) {
  doPlay = doPlay || true;
  var videos = stack[stack.length-1].videos;
  var startIndex = 1;
  if (videos != null) {
    startIndex = videos.data.startIndex + videos.data.items.length;
  }
  getPage(startIndex);
}

function previousPage() {
  var videos = stack[stack.length-1].videos;
  if (videos.data.startIndex > 1) {
    var startIndex = Math.max(1, videos.data.startIndex - videos.data.itemsPerPage);
    getPage(startIndex);
  }
}

function getPage(startIndex, doPlay) {
  doPlay = doPlay || true;
  $.ajax({
    type: 'GET',
    url: stack[stack.length-1].url,
    data: {
    },
    dataType: 'jsonp',
    success: function(data) {
      if (stack[stack.length-1].videos !== null) {
        stack[stack.length-1].videos.data.items  = stack[stack.length-1].videos.data.items.concat(data.data.items);
        stack[stack.length-1].videos.data.itemsPerPage += data.data.itemsPerPage;
        refreshPlaylist(data.data.startIndex - stack[stack.length-1].videos.data.startIndex);
        if (doPlay) {
          playVideo();
        }
      }
      else {
        if (data.videos.total > 0) {
          stack[stack.length-1].videos = data.videos;
          stack[stack.length-1].current = 0;
          refreshPlaylist();
          if (doPlay) {
            playVideo(); 
          }
        }
        else {
          stack.pop();
        }
      }
    },
    error: function(a,b,c) {
    }
  });
}

function refreshPlaylist(startIndex) {
  startIndex = startIndex || 0;
  var videos = stack[stack.length-1].videos;
  if (!startIndex) {
    $("#playlist ul").html('');
  }
  $.each(videos.data.items, function(i, item) {
    if (i >= startIndex) {
      if (typeof(item.video) != "undefined") { // playlist entry
        stack[stack.length-1].videos.data.items[i] = item = item.video;
      }
      index = videos.data.startIndex + i; 
      $("#playlist ul").append('<li><img id="item-' + index + '" src="' + item.thumbnail.sqDefault + '" onclick="watchVideo(' + i + ');" title="' + item.title + '" /></li>');
    }
  });
}

function watchVideo(i) {
  stack[stack.length-1].current = i;
  playVideo();
}

/*
function getYouTubeQuality(width, height) {
  if (height <= 240) {
    quality = 'small';
  }
  else if (height <= 360) {
    quality = 'medium';
  }
  else if (height <= 480) {
    quality = 'large';
  }
  else if (height <= 720) {
    quality = 'hd720';
  }
  else if (height <= 1080) {
    quality = 'hd1080';
  }
  else {
    quality = 'highres';
  }
  return quality;
}
*/
function vimeo_player_loaded(playerid) {
  player = document.getElementById("myvimeoplayer");
  var width = $(player).parent().width();
  var height = width * 9 / 16;
  $(player).width(width).height(height);
  player.api_addEventListener("finish", function(data) { nextVideo(); });
  player.api_addEventListener("play", function(data) { $("#controls #pause").removeAttr('disabled').html("Pause"); });
  player.api_addEventListener("pause", function(data) { $("#controls #pause").removeAttr('disabled').html("Play"); });
  player.api_setVolume(0);

  // Start by watching Drupal.
  watchNew('http://vimeo.com/api/rest/v2?format=json&method=vimeo.videos.search&query=drupal');
}

function pauseVideo() {
  if (player.api_paused()) {
    player.api_play();
  }
  else {
    player.api_pause();
  }
}

function watchNew(url) {
  stack.push({
    url: url,
    videos: null,
    current: 0,
  });
  nextPage();
}

function watchChannel(i) {
  var entry = channels.feed.entry[i];
  watchNew(typeof(entry.content) != "undefined" ? entry.content.src : 'http://gdata.youtube.com/feeds/api/users/' + entry['yt$username']['$t'] + '/uploads');
}

function watchUser() {
  var videos = stack[stack.length-1].videos;
  var current = stack[stack.length-1].current;
  watchNew('http://gdata.youtube.com/feeds/api/users/' + videos.data.items[current].uploader + '/uploads');
}

function watchRelated() {
  var videos = stack[stack.length-1].videos;
  var current = stack[stack.length-1].current;
  watchNew('http://gdata.youtube.com/feeds/api/videos/' + videos.data.items[current].id + '/related');
}

function watchTags() {
  var url = 'http://gdata.youtube.com/feeds/api/videos/-';
  $('#status #tags input:checkbox:checked').each(function() {
    url += '/' + $(this).val();
  });
  watchNew(url);
}

function watchSearch() {
  watchNew('http://gdata.youtube.com/feeds/api/videos?q=' + encodeURI($('#controls #query').val()));
}

function watchFeed() {
  watchNew($('#status #feed').val());
}

function saveFeed() {
  var videos = stack[stack.length-1].videos;
  var title = $('#save-name').val();
  var description = typeof(videos.data.description) != "undefined" ? videos.data.description : "";
  var feed = stack[stack.length-1].url;
  var source = 'youtube';

  $.ajax({
    type: 'POST',
    url: Drupal.settings.basePath + 'feedeo/js/add',
    data: 'title=' + escape(title) + '&description=' + escape(description) + '&feed=' + escape(feed) + '&source=' + escape(source),
    datatype: 'json',
    async: false,
    success: function(data) {
      var result = Drupal.parseJson(data);
      $('#saved').
        append($("<option></option>").
        attr("value", result['nid']).
        text(result['title']));
      $('#saved').val(result['nid']);
    }
  });
}

function onSaveChange() {
  value = $("#save-name").val();
  if (value.length > 0) {
    $("#save").removeAttr('disabled');
  }
  else {
    $("#save").attr('disabled', 'disabled');
  }
}

function onQueryChange() {
  value = $("#controls #query").val();
  if (value.length > 0) {
    $("#controls #search").removeAttr('disabled');
  }
  else {
    $("#controls #search").attr('disabled', 'disabled');
  }
}

