<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function feedeo_ctools_plugin_api($owner, $api) {
  if ($owner == 'feedeo' && $api == 'plugins') {
    return array('version' => 1);
  }
}

/**
 * Implementation of hook_feedeo_plugins().
 */
function feedeo_feedeo_plugins() {
  $info = array();
  $path = drupal_get_path('module', 'feedeo') . '/plugins';
  $info['base'] = array(
    'handler' => array(
      'class' => 'FeedeoPlugin',
      'file' => 'FeedeoPlugin.inc',
      'path' => $path,
    ),
  );
  $info['youtube'] = array(
    'title' => 'YouTube',
    'handler' => array(
      'parent' => 'base',
      'class' => 'FeedeoYouTube',
      'file' => 'FeedeoYouTube.inc',
      'path' => $path,
    ),
  );
  $info['dailymotion'] = array(
    'title' => 'Dailymotion',
    'handler' => array(
      'parent' => 'base',
      'class' => 'FeedeoDailymotion',
      'file' => 'FeedeoDailymotion.inc',
      'path' => $path,
    ),
  );
  return $info;
}

/**
 * Gets all available plugins. Does not list hidden plugins.
 *
 * @return
 *   An array where the keys are the plugin keys and the values
 *   are the plugin info arrays as defined in hook_feeds_plugins().
 */
function feedeo_get_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('feedeo', 'plugins');
}

/**
 * Instantiate a new plugin given its key.
 */
function feedeo_factory($plugin_key, $values = array()) {
  module_load_include('inc', 'feedeo', 'plugins/FeedeoPlugin');
  return FeedeoPlugin::factory($plugin_key, $values);
}

/**
 * Implementation of hook_menu().
 */
function feedeo_menu() {
  $items = array();

  $items['admin/settings/feedeo'] = array(
    'title' => 'Feedeo',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('feedeo_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );
  $items['admin/settings/feedeo/general'] = array(
    'title' => 'General',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  ctools_include('plugins');
  $plugins = ctools_get_plugins('feedeo', 'plugins');
  foreach ($plugins as $plugin_key => $info) {
    if (isset($info['title'])) {
      $items['admin/settings/feedeo/' . $plugin_key] = array(
        'title' => $info['title'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('feedeo_admin_plugin', 3),
        'access arguments' => array('administer site configuration'),
        'type' => MENU_LOCAL_TASK,
      );
      $items['feedeo/' . $plugin_key] = array(
        'title' => 'Watch ' . $info['title'],
        'page callback' => 'theme',
        'page arguments' => array('feedeo', 1),
        'access callback' => TRUE,
      );
    }
  }
  $items['feedeo/js/channels/add'] = array(
    'title' => 'Add channel',
    'page callback' => 'feedeo_add_channel',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['feedeo/js/channels/get'] = array(
    'title' => 'Get channels',
    'page callback' => 'feedeo_get_channels',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['feedeo/js/bookmarks/add'] = array(
    'title' => 'Add bookmark',
    'page callback' => 'feedeo_add_bookmark',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['feedeo/js/bookmarks/get'] = array(
    'title' => 'Get bookmarks',
    'page callback' => 'feedeo_get_bookmarks',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['node/%feedeo_channel/watch'] = array(
    'title' => 'Watch',
    'page callback' => 'feedeo_channel_watch',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implementation of hook_link().
 */
function feedeo_link($type, $object, $teaser = FALSE) {
  $links = array();

  if ($type == 'node' && $object->type == 'channel') {
    $links['feedeo'] = array(
      'title' => t('Watch'),
      'href' => 'node/' . $object->nid . '/watch',
    );
  }

  return $links;
}

/**
 * Load function for channel.
 */
function feedeo_channel_load($nid) {
  $node = node_load($nid);
  return $node->type == 'channel' ? $node : FALSE;
}

/**
 * Page callback to watch channel.
 */
function feedeo_channel_watch($channel) {
  drupal_set_title($channel->title);
  return theme('feedeo', $channel->field_source[0]['value'], $channel->field_feed[0]['value']);
}

/**
 * Form callback for Feedeo general settings.
 */
function feedeo_admin_settings() {
  $form['feedeo_items_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Items per page'),
    '#default_value' => variable_get('feedeo_items_per_page', 25),
  );
  return system_settings_form($form);
}

/**
 * Form callback for Feedeo plugin settings.
 */
function feedeo_admin_plugin($form_state, $plugin_key) {
  $form['feedeo_' . $plugin_key . '_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('feedeo_' . $plugin_key . '_username', ''),
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_theme().
 */
function feedeo_theme($existing, $type, $theme, $path) {
  return array(
    'feedeo' => array(
      'arguments' => array(
        'plugin_key' => array('default' => NULL),
        'feed' => array('default' => NULL),
      ),
      'template' => 'feedeo',
    ),
  );
}

/**
 * Preprocess function for theme('feedeo').
 */
function feedeo_preprocess_feedeo(&$vars) {
  $plugin_key = $vars['plugin_key'];
  $feed = is_string($vars['feed']) ? $vars['feed'] : NULL;
  drupal_add_css(drupal_get_path('module', 'feedeo') . '/feedeo.css');
  drupal_add_js(drupal_get_path('module', 'feedeo') . '/js/feedeo.js');
  drupal_add_js(drupal_get_path('module', 'feedeo') . '/js/underscore-min.js');
  drupal_add_js(array('feedeo' => array(
    'source' => $plugin_key,
    'feed' => $feed,
    'user' => variable_get('feedeo_' . $plugin_key . '_username', ''),
  )), 'setting');
  $plugin = feedeo_factory($plugin_key);
  $plugin->add_js();
}

/**
 * AJAX function to add a new channel.
 */
function feedeo_add_channel() {
  module_load_include('inc', 'node', 'node.pages');

  $title = $_REQUEST['title'];
  $description = $_REQUEST['description'];
  $feed = $_REQUEST['feed'];
  $source = $_REQUEST['source'];

  global $user;
  $channel = new StdClass;
  $channel->type = 'channel';
  node_object_prepare($channel);
  $channel->title = $title;
  $channel->body = $description;
  $channel->name = $user->name;
  $channel->field_feed[0]['value'] = $feed;
  $channel->field_source[0]['value'] = $source;
  node_save(node_submit($channel));
  drupal_json(array('nid' => $channel->nid, 'title' => $title));
  exit();
}

/**
 * AJAX function to retrieve all channels by source.
 */
function feedeo_get_channels() {
  global $user;
  $uid = $_REQUEST['uid'] || $user->uid;
  $source = $_REQUEST['source'];
  $result = db_query("
SELECT c.field_feed_value, n.title
FROM {node} n
INNER JOIN {content_type_channel} c ON c.nid = n.nid
WHERE
  type = 'channel'
  AND uid = %d
  AND field_source_value = '%s'
ORDER BY created ASC
  ", $uid, $source);
  $channels = array();
  while ($channel = db_fetch_object($result)) {
    $channels[] = array('feed' => $channel->field_feed_value, 'title' => $channel->title);
  }
  drupal_json($channels);
  exit();
}

/**
 * AJAX function to add a new bookmark.
 */
function feedeo_add_bookmark() {
  module_load_include('inc', 'node', 'node.pages');

  $title = $_REQUEST['title'];
  $comment = $_REQUEST['comment'];
  $video = $_REQUEST['video'];
  $time = $_REQUEST['time'];

  global $user;
  $bookmark = new StdClass;
  $bookmark->type = 'bookmark';
  node_object_prepare($bookmark);
  $bookmark->title = $title;
  $bookmark->body = $comment;
  $bookmark->name = $user->name;
  $bookmark->field_video[0]['value'] = $video;
  $bookmark->field_time[0]['value'] = $time;
  node_save(node_submit($bookmark));
  drupal_json(array('time' => $time, 'title' => $title, 'comment' => $comment));
  exit();
}

/**
 * AJAX function to retrieve all bookmarks by video.
 */
function feedeo_get_bookmarks() {
  $video = $_REQUEST['video'];
  $result = db_query("
SELECT b.field_time_value, n.title, nr.body
FROM {node} n
INNER JOIN {content_type_bookmark} b ON b.nid = n.nid
INNER JOIN {node_revisions} nr ON nr.vid = n.vid
WHERE
  type = 'bookmark'
  AND field_video_value = '%s'
ORDER BY b.field_time_value ASC
  ", $video);
  $bookmarks = array();
  while ($bookmark = db_fetch_object($result)) {
    $bookmarks[] = array(
      'time' => $bookmark->field_time_value,
      'title' => $bookmark->title,
      'comment' => $bookmark->body,
    );
  }
  drupal_json($bookmarks);
  exit();
}
