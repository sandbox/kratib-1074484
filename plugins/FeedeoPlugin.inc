<?php

abstract class FeedeoPlugin {
  /**
   * Instantiate, populate and return a plugin object.
   *
   * @param $plugin_key
   *
   * @param $values
   *   An array with at least a plugin_key key identifying the plugin class to
   *   use for instantiating this plugin.
   */
  public static function factory($plugin_key, $values) {
    ctools_include('plugins');
    if ($class = ctools_plugin_load_class('feedeo', 'plugins', $plugin_key, 'handler')) {

      // While we actually prefer to get objects, we need to allow for either,
      // so we convert it all to arrays.
      if (is_object($values)) {
        $values = (array)$values;
      }

      $plugin = new $class();
      $plugin->plugin_key = $plugin_key;

      foreach ($plugin as $key => $value) {
        if (isset($values[$key])) {
          $plugin->$key = $values[$key];
        }
      }
      foreach ($plugin->options_defaults() as $key => $value) {
        if (isset($values[$key])) {
          $plugin->options[$key] = $values[$key];
        }
      }
      return $plugin;
    }
    return FALSE;
  }

  /**
   * Create a new plugin.
   */
  protected function __construct() {
    $this->options = $this->options_defaults();
  }

  /**
   * Declare default options.
   */
  public function options_defaults() { return array(); }

  /**
   * Provide options to configure content.
   */
  public function options_form() {}

  /**
   * Add necessary JavaScript files.
   */
  abstract public function add_js();
}

