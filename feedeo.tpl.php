<script src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<div id="feedeo">
<div id="player"></div>

<div id="messages-status" class="messages status"><ul></ul></div>
<div id="messages-error" class="messages error"><ul></ul></div>

<div id="controls">
<button id="previous" disabled>Previous</button>
<button id="next" disabled>Next</button>
<button id="pause" disabled>Pause</button>
<button id="mute" disabled>Unmute</button>
<button id="pop" disabled>Backtrack</button>
<input type="textfield" id="query"></input>
<button id="search" disabled>Search</button>
<button id="bookmark" disabled>Bookmark</button>
<div id="bookmark-dialog">
  <textarea id="bookmark-comment" placeholder="Your comment here"></textarea>
  <button id="bookmark-save" disabled>Save Bookmark</button>
</div>
<select id="bookmarks"><option value='0'>- BOOKMARKS -</option></select>
</div>

<div id="status">
<ul>
<li>Feed: <input type="textfield" id="feed" size="60"><button id="feed-button">Watch</button></li>
<li>Name: <input type="textfield" id="save" size="60"><button id="save-button" disabled>Save</button>
<select id="saved"><option value='0'>- SELECT -</option></select></li>
<li>Index: <span id="index">index</span></li>
<li>Title: <span id="title">title</span><button id="related-button">Watch Related</button></li>
<li>Description: <span id="description">description</span></li>
<li>User: <span id="user">user</span><button id="user-button">Watch User Channel</button></li>
<li>Tags: <span id="tags">tags</span><button id="tags-button">Watch Tags</button></li>
</ul>
</div>

<div id="playlist" class="feedeo-list">
<h2>Playlist</h2>
<ul>
</ul>
<button id="more-button">More Videos</button>
</div>

<div id="channels" class="feedeo-list">
<h2>Channels</h2>
<ul>
</ul>
</div>

<div id="history" class="feedeo-list">
<h2>History</h2>
<ul>
</ul>
</div>
</div>
